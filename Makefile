SHELL = /bin/sh

.DEFAULT_GOAL := help

# This will output the help for each task.
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	@echo "\n  Allowed for overriding next properties:\n\n\
		Usage example:\n\
	    	make up"


linux-install-java: ## Linux install java
	sudo apt install openjdk-11-jdk

scan: gradle sonar scan, use special key(login)
	./gradlew --build-cache sonarqube -Dsonar.login= -Dsonar.host.url=

build: ## gradlew build ,add cache
	./gradlew --build-cache

task-copy-natives: ## gradle copy natives(readme)
	./gradlew copyNatives

run-app: ## gradle run app
	./gradlew run
